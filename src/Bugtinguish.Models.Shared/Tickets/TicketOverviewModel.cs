namespace Bugtinguish.Models.Shared;

public class TicketOverviewModel
{
    public Guid Id { get; set; }
    public int Number { get; set; }
    public string? Tracker { get; set; }
    public string? Assignee { get; set; }
    public string? Created { get; set; }
    public string? Description { get; set; }
    public int Progress { get; set; }
}
