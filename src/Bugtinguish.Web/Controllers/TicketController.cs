using System.Diagnostics;
using Bugtinguish.Models.Web;
using Bugtinguish.Models.Shared;
using Microsoft.AspNetCore.Mvc;

namespace bugtinguish.Web.Controllers;
    
public class TicketController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public TicketController(ILogger<HomeController> logger)
    {
       _logger = logger; 
    }

    public IActionResult Index()
    {
        List<TicketOverviewModel> tickets = new();
        for (int i = 0; i < 10; i++)
        {
            tickets.Add(new()
            {
                Id = Guid.NewGuid(),
                Number = i,
                Tracker = "New",
                Created = DateTime.Now.ToShortDateString(),
                Assignee = "Jean Patrick Kocherscheidt",
                Progress = 0,
                Description = "A first glimpse of how tickets should look",
            });
        }

        return View(tickets);
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
